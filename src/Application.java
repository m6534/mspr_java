import fr.epsi.mspr.Agent;

import java.io.*;
import java.security.NoSuchAlgorithmException;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.util.*;
import java.util.stream.Stream;


public class Application {
    // TODO faire les tests

    /**
     * Read a txt file
     *
     * @param filepath the path of the file to read
     * @return an arraylist containing the file lines
     */
    public static ArrayList<String> readFile(String filepath) throws IOException {
        BufferedReader bufReader = new BufferedReader(new FileReader(filepath, StandardCharsets.UTF_8));
        ArrayList<String> listOfLines = new ArrayList<>();

        String line = bufReader.readLine();
        while (line != null) {
            listOfLines.add(line);
            line = bufReader.readLine();
        }

        bufReader.close();

        return listOfLines;
    }

    /**
     * Get equipment list from a txt file
     *
     * @param equipmentFilepath the path of the equipment file
     * @return a list of map <equipment code name, equipment full name>
     */
    public static Map<String, String> getEquipmentListFromFile(String equipmentFilepath) throws IOException {
        ArrayList<String> equipmentLines = readFile(equipmentFilepath);

        Map<String, String> equipmentMap = new HashMap<>();
        for (String line : equipmentLines) {
            String[] elements = line.split(" ", 2);
            String code = elements[0];
            String description = elements[1];
            equipmentMap.put(code, description);
        }

        return equipmentMap;
    }

    /**
     * Get agent info from a txt file
     *
     * @param agentFilePath the path of the agent file
     * @return arraylist of 2 lists : 1 list with [firstName, lastName, mission, password],
     * another list with containing equipment codes
     */
    public static ArrayList<List<String>> getAgentInfoFromFile(String agentFilePath) throws IOException {
        ArrayList<String> agentLines = readFile(agentFilePath);
        List<String> agentDetails = agentLines.subList(0, 4);
        List<String> agentEquipments = agentLines.subList(5, agentLines.size());
        ArrayList<List<String>> res = new ArrayList<>();
        res.add(agentDetails);
        res.add(agentEquipments);
        return res;
    }

    /**
     * Use data files to generate html files and a htpasswd file
     *
     * @param inputDirectory path of the directory containing the data files
     * @param staffFilename name of the file containing the list of staff members
     * @param equipmentFilename  name of the file containing the list of equipments
     *
     */
    public static void data2html(String inputDirectory, String outputDirectory, String staffFilename, String equipmentFilename) throws IOException, NoSuchAlgorithmException {
        String staffFile = String.format("%s%s", inputDirectory, staffFilename);
        String equipmentFile = String.format("%s%s", inputDirectory, equipmentFilename);
        // TODO verify file format

        // equipment file
        Map<String, String> equipmentMap = getEquipmentListFromFile(equipmentFile);

        // htpasswd file
        Map<String, String> passwordMap = new HashMap<>();

        // copy logo to output directory
        String logoFilename = "logo.png";
        String logoInputPath = String.format("%s%s", inputDirectory, logoFilename);
        String logoOutputPath = String.format("%s%s", outputDirectory, logoFilename);
        Files.copy(Path.of(logoInputPath), Path.of(logoOutputPath), StandardCopyOption.REPLACE_EXISTING );

        // staff file
        List<String> agentIdList = readFile(staffFile);
        List<Agent> agentList = new ArrayList<>();

        for (String agentId : agentIdList) {
            String agentPicturePath = String.format("%s%s.jpg", inputDirectory, agentId);

            Agent agent = createAgentInstance(inputDirectory, agentId, equipmentMap);
            agentList.add(agent);

            // append agent password hash
            passwordMap.put(agent.getId(), stringToSHA256(agent.getPassword()));

            createHtmlAgentPage(agent, agentPicturePath, outputDirectory);
        }
        agentList.sort(Comparator.comparing(Agent::getLastName));
        createHtmlHomePage(agentList, outputDirectory);

        // create htpasswd file
        writeHtPasswordFile(passwordMap, outputDirectory);
    }

    /**
     * Write htpasswd file and put it into output directory
     *
     * @param passwordMap map containing userId and password
     * @param outputDirectory directory where the file is written
     */
    private static void writeHtPasswordFile(Map<String, String> passwordMap, String outputDirectory) throws IOException {
        StringBuilder htpasswdString = new StringBuilder();
        for (var entry : passwordMap.entrySet()) {
            // format : "userId : hashedPassword" (SHA-256)
            htpasswdString.append(String.format("%s: %s%n", entry.getKey(), entry.getValue()));
        }

        // generate password file and store it to output directory
        File htpasswdFile= new File(String.format("%s.htpasswd", outputDirectory));
        writeStringToFile(htpasswdFile, String.valueOf(htpasswdString));
    }

    /**
     * Hash a string with SHA-256
     *
     * @param string string to encode and hash
     * @return hashed string
     */
    private static String stringToSHA256(String string) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] encodedhash = digest.digest(string.getBytes(StandardCharsets.UTF_8));
        return bytesToHex(encodedhash);
    }

    /**
     * Convert byte array to hex
     *
     * @param hash byte array
     * @return hex
     */
    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder(2 * hash.length);
        for (byte b : hash) {
            String hex = Integer.toHexString(0xff & b);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    /**
     * Generate an agent instance from an agent id
     *
     * @param inputDirectory path of the directory containing the data files
     * @param agentId the id of the agent
     * @param equipmentMap a list of map <equipment code name, equipment full name>
     * @return an instance of Agent containing the right info
     */
    private static Agent createAgentInstance(String inputDirectory, String agentId, Map<String, String> equipmentMap) throws IOException {
        String agentFile = String.format("%s%s.txt", inputDirectory, agentId);
        ArrayList<List<String>> agentInfo = getAgentInfoFromFile(agentFile);

        Agent agent = new Agent(agentId);
        agent.setLastName(agentInfo.get(0).get(0));
        agent.setFirstName(agentInfo.get(0).get(1));
        agent.setMission(agentInfo.get(0).get(2));
        agent.setPassword(agentInfo.get(0).get(3));

        List<String> equipmentList = new ArrayList <>();
        for (String equipment : agentInfo.get(1)) {
            equipmentList.add(equipmentMap.get(equipment));
        }
        agent.setEquipments(equipmentList);

        return agent;
    }

    /**
     * Create the html homepage based on the files data
     *
     * @param agentList the list of agents
     * @param outputDirectory the directory containing output html file
     */
    private static void createHtmlHomePage(List<Agent> agentList, String outputDirectory) throws IOException {
        // generate an item for each equipment and put it into a list
        StringBuilder listElements = new StringBuilder();
        for (Agent agent : agentList) {
            String agentId = agent.getId();
            String fullName = String.format("%s %s", agent.getLastName().toUpperCase(), agent.getFirstName());
            String listLine = String.format("\t\t\t<a href=\"./fiche_%s.html\" class=\"button\">%s</a><div></div>", agentId, fullName);
            listElements.append(listLine);
        }

        // set info in html template
        File htmlTemplateFile = new File("src/fr/epsi/mspr/homepageTemplate.html");
        String htmlString = readFileToString(htmlTemplateFile);
        htmlString = htmlString.replace("$listElements", listElements.toString());

        // generate html page and store it to the server
        File newHtmlFile = new File(String.format("%sgoSecuriHomepage.html", outputDirectory));
        writeStringToFile(newHtmlFile, htmlString);
    }

    /**
     * Create the html info page for an agent
     *
     * @param agent an instance of agent
     * @param agentPicturePath the path of the agent picture
     * @param outputDirectory the directory containing output html file
     */
    private static void createHtmlAgentPage(Agent agent, String agentPicturePath, String outputDirectory) throws IOException {
        // generate an item for each agent and put it into a list
        StringBuilder listElements = new StringBuilder();
        for (String equipment : agent.getEquipments()) {
            String listLine = String.format("<li>%s</li>\n", equipment);
            listElements.append(listLine);
        }

        // send image to output directory
        String fileSeparator = File.separator;
        String pictureFilename = agentPicturePath.substring(agentPicturePath.lastIndexOf(fileSeparator) + 1);
        String agentOutputAgentPath = String.format("%s%s", outputDirectory, pictureFilename);
        Files.copy(Path.of(agentPicturePath), Path.of(agentOutputAgentPath), StandardCopyOption.REPLACE_EXISTING );

        // set info in html template
        File htmlTemplateFile = new File("src/fr/epsi/mspr/agentPageTemplate.html");
        String htmlString = readFileToString(htmlTemplateFile);
        String homepage = "./goSecuriHomepage.html";
        String fullName = String.format("%s %s", agent.getLastName().toUpperCase(), agent.getFirstName());
        htmlString = htmlString.replace("$homepage", homepage);
        htmlString = htmlString.replace("$fullname", fullName);
        htmlString = htmlString.replace("$mission", agent.getMission());
        htmlString = htmlString.replace("$agentPicturePath", pictureFilename);
        htmlString = htmlString.replace("$listElements", listElements);

        // generate html page and store it to the server
        File newHtmlFile = new File(String.format("%sfiche_%s.html", outputDirectory, agent.getId()));
        writeStringToFile(newHtmlFile, htmlString);
    }

    /**
     * Read template and convert it to string
     *
     * @param htmlTemplateFile the html file to convert
     * @return the string content of the file
     */
    private static String readFileToString(File htmlTemplateFile) {
        StringBuilder contentBuilder = new StringBuilder();

        try (Stream<String> stream = Files.lines( Paths.get(htmlTemplateFile.getPath()), StandardCharsets.UTF_8))
        {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return contentBuilder.toString();
    }

    /**
     * Generate a static html page from a string
     *
     * @param newHtmlFile output file path
     * @param htmlString string to convert into a file
     */
    private static void writeStringToFile(File newHtmlFile, String htmlString) throws IOException {
        Path fileName = Path.of(newHtmlFile.getPath());

        try (Writer out = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(newHtmlFile), StandardCharsets.UTF_8))) {
            out.write(htmlString);
        }

        System.out.printf("Finish writing file %s\n", fileName);
    }

    /**
     * Format directory path
     *
     * @param directory path to check
     * @return corrected directory path
     */
    private static String formatDirectory(String directory) {
        String directoryToReturn = "";
        String fileSeparator = File.separator;
        if (!directory.endsWith(fileSeparator)) {
            directoryToReturn = directory + fileSeparator;
        }
        return directoryToReturn;
    }

    public static void main(String[] args) throws IOException, NoSuchAlgorithmException {
        String inputDirectory = formatDirectory(args[0]);
        String outputDirectory = formatDirectory(args[1]);
        String staffFile = args[2];
        String equipmentFile = args[3];
        data2html(inputDirectory, outputDirectory, staffFile, equipmentFile);
    }
}

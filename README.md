# GoSecuri txt2html

## About

The company currently has equipment management software which data is saved on a Git repository in text format.
This repository also stores the list of agents and scans of their IDs.

A Web solution makes it possible to consult the files of the agents and the equipment in their possession.
This site consists of static pages generated from files in the Git repository.

The generation is performed by this Java application.

## Description

Generates html files from input files and a ```.htpasswd``` containing hashed login info.

The application uses the following files as input: 
- agent id list ```staff.txt```
- equipment list ```liste.txt```
- individual agent info ```<agent_id>.txt```
- individual agent picture ```<agent_id>.jpg```


## Usage

- Prerequisite: Java 17
  - `` $ java --version``
  - ``> Openjdk 17.x.x``
  - ``> OpenJDK Runtime Environment (build 17.x.x)``
- Compilation: In the project root directory
  - ```javac ./src/Application.java  ./src/fr/epsi/mspr/Agent.java```
- Packaging: In the project root directory
  - ```jar cfmv ./Application.jar ./src/Manifest.txt ./src/Application.class ./src/fr/epsi/mspr/Agent.class```
- Execution: In the project root directory
  - ```java -jar ./Application.jar <input-directory> <output-directory> staff.txt liste.txt```
